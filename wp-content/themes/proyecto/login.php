<?php
/*
  Template Name: Login
 */

get_header();
?>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?> 

        <div class="form-contact2">

            <?php if (is_user_logged_in()) { ?>

                <h2 class="modal-title" id="myModalLabel">Mi Perfil</h2>

            <?php } else { ?>

                <h2 class="modal-title" id="myModalLabel"><?php the_title(); ?></h2>

            <?php } ?>

        </div>
        <div class="modal-body">

            <?php
            if (is_user_logged_in()) {

                global $current_user;
                get_currentuserinfo();
                ?>


                <br /> <br />

                <form id="profile_form" name="profile_form" class="form-contact2">
                    <br />
                    <div id="contenido2" class="row">
                        <div class=" izq">
                            <div class="control-group">
                                <p>
                                    <?php echo get_avatar($current_user->user_email) ?>
                                </p>
                                <p>
                                    <label class="perfil">Nombre:</label>
                                    <?php echo $current_user->user_firstname ?>
                                </p>
                            </div>
                            <div class="control-group">
                                <p>
                                    <label class="perfil">Apellidos:</label>
                                    <?php echo $current_user->user_lastname ?>
                                </p>
                            </div>
                            <div class="control-group">
                                <p>
                                    <label class="perfil">Email:</label>
                                    <?php echo $current_user->user_email ?>
                                </p>
                            </div>
                            <div class="control-group">
                                <p>
                                    <label class="perfil">ID Usuario:</label>
                                    <?php echo $current_user->ID ?>
                                </p>
                            </div>
                            <div class="control-group">
                                <p>
                                    <label class="perfil">Nivel:</label>
                                    <?php echo $current_user->user_level ?>
                                </p>
                            </div>
                            <div  class="control-group logout">
                                <a href="<?php echo wp_logout_url(home_url()); ?>">Logout</a>
                            </div>
                        </div>        
                    </div>
                </form>

            <?php } else { ?>

                <form id="login_form" name="login_form" class="form-contact3" method="post" action="<?php bloginfo('url') ?>/wp-login.php">

                    <div class="control-group">
                        <input type="text" required id="user" name="log" placeholder="Usuario" class="input-block-level" dir="auto" maxlength="100">
                    </div>
                    <div class="control-group">
                        <input required type="password" id="pass" name="pwd" placeholder="Contraseña" class="input-block-level" maxlength="100">
                    </div>

                    <input class="btn btn-primary" type="submit" name="user-submit" id="submitLog" value="Enviar" />

                    <input type="hidden" name="redirect_to" value="<?php bloginfo('url') ?>" />
                    <input type="hidden" name="user-cookie" value="1" />
                </form>

                <div class="social form-contact3">

                    <p>Ó</p>

                    <ul class="icons log">
                        <li>
                            <a class="icon rounded fa-facebook" id="fb" href="<?php bloginfo('url'); ?>/#"><span class="label">Facebook</span></a>
                        </li>

                        <li>
                            <a class="icon rounded fa-twitter" id="twlogin" href="<?php bloginfo('url'); ?>/#"><span class="label">Twitter</span></a>
                        </li>
                    </ul>

                </div>
                <div class="form-contact reg ">
                    <p>¿Aun no te has registrado? Hazlo <a href="<?php bloginfo('url'); ?>/registro" id="linkReg">aquí</a></p>
                    <p><a href="<?php bloginfo('url'); ?>/wp-login.php?action=lostpassword" id="linkRest">¿Has olvidado tu contraseña?</a></p>
                </div>

            </div>

            <div class="form-contact2"><div class="8u 12u$(medium)">
                    <div class="copyright">&copy; 2016 JoinElderly. All rights reserved.</div>
                </div>
            </div><!--</div>
            </div>-->
        <?php } ?>
        <?php
    }
}
?>  

<?php get_footer(); ?>


