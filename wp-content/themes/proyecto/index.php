<?php get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/assets/css/style_slider.css" />

<div id="wowslider-container1">
    <div class="ws_images"><ul>
            <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/relacionate.jpg" alt="Relaciónate" title="Relaciónate" id="wows1_0"/></li>
            <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/vivemejor.jpg" alt="Vive mejor" title="Vive mejor" id="wows1_1"/></li>
            <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/salir.jpg" alt="Sal con más gente" title="Sal con más gente" id="wows1_2"/></li>
            <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/diviertete.jpg" alt="Diviértete" title="Diviértete" id="wows1_3"/></li>
        </ul></div>
    <div class="ws_bullets"><div>
            <a href="#" title="Relaciónate"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/tooltips/relacionate.jpg" alt="Relaciónate"/>1</span></a>
            <a href="#" title="Vive mejor"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/tooltips/vivemejor.jpg" alt="Vive mejor"/>2</span></a>
            <a href="#" title="Sal con más gente"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/tooltips/salir.jpg" alt="Sal con más gente"/>3</span></a>
            <a href="#" title="Diviértete"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/tooltips/diviertete.jpg" alt="Diviértete"/>4</span></a>
        </div></div>
    <div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/wowslider.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/script.js"></script>

<!-- One -->
<section id="one" class="wrapper style1 special">
    <div class="container">
        <header class="major">
            <h2>JoinElderly.com</h2>
            <p>Tu página para buscar, conocer y encontrar una nueva forma de atención.</p>
        </header>
        <div class="row 150%">
            <div class="4u 12u$(medium)">
                <section class="box">
                    <a href="ofertas"> <img id="img_contact" src="<?php echo get_stylesheet_directory_uri();?>/assets/media/buscar.jpg" alt="120" width="120"></a>
                    <h3>Busca acompañante</h3>
                    <p>Encuentra las mejores personas para que te acompañen o unete a un grupo con más gente de tu edad.</p>
                </section>
            </div>
            <div class="4u 12u$(medium)">
                <section class="box">
                    <a href="trabaja"> <img id="img_contact" src="<?php echo get_stylesheet_directory_uri();?>/assets/media/trabaja.png" alt="120" width="120"></a>
                    <h3>Únete a nosotros</h3>
                    <p>No te quedes encerrado en casa, únete a nosotros, sal a la calle y empieza a relacionarte con gente, nunca ha sido tan fácil como ahora.</p>
                </section>
            </div>
            <div class="4u$ 12u$(medium)">
                <section class="box">
                    <a href="contacto"><img id="img_contact" src="<?php echo get_stylesheet_directory_uri();?>/assets/media/logo-contact.png" alt="120" width="120"></a>
                    <h3>Contacta con nosotros</h3>
                    <p>Para cualquier duda, sugerencia o problema solo necesitas rellenar estos campos y ya estará todo hecho, cuanto más simple, mejor.</p>
                </section>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>