<?php

/*
Template Name: Ofertas
*/

get_header();?>

<section id="main" class="wrapper">
    <div class="container">

    <?php 
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post(); ?> 
                        <header class="major">
                            <h2><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                        </header>
                <?php } 
            } 
    ?> 
        <div class="image fit">

            <div id='ubicacion'></div>

            <div id="mapholder">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d395898.1744546494!2d-0.5655122774293887!3d39.175755365654545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6177ff60c5fb55%3A0x989f7b3f238bd31!2s46870+Onteniente%2C+Valencia!5e0!3m2!1ses!2ses!4v1462372177242" width="700" height="650" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <div class="ofertas"> 
                
                 <?php
                    // WP_Query arguments
                    $args = array (
                        'post_type'              => array( 'ofertas' ),
                    );

                    // The Query
                    $ofertas = new WP_Query( $args );

                    // The Loop
                    if ( $ofertas->have_posts() ) {
                        while ( $ofertas->have_posts() ) {
                            $ofertas->the_post();  ?>
                            
                            <?php 

                                $date_inicio = get_field('fecha_inicio', false, false); 
                                $date_inicio = new DateTime($date_inicio);

                                $date_final = get_field('fecha_final', false, false); 
                                $date_final = new DateTime($date_final);

                            ?>

                            <div class="of">
                                <div ng-click="select(of.id)">
                                    <div class="desc"><?php the_title(); ?></div>
                                    <div class="fecha"> Fecha: <?php echo $date_inicio->format('j M Y'); ?> - <?php echo $date_final->format('j M Y'); ?></div>
                                    <div class="hora"> Hora: <?php the_field('hora_inicio'); ?> - <?php the_field('hora_final'); ?> h</div>
                                    <div class="precio"> Precio: <?php the_field('precio'); ?> €</div>
                                    <div class="details" ><a href='<?php the_permalink(); ?>'>Ver más</a></div>
                                </div>
                            </div>
                        <?php }  
                    } 

                    // Restore original Post Data
                    wp_reset_postdata();

                    ?>   
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>