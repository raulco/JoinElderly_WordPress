<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>

        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--Favicon-->
        <link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri();?>/assets/media/favicon.ico">
        <!--JS-->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/skel.min.js"></script>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/skel-layers.min.js"></script>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/init.js"></script>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/arriba.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!--CSS -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/assets/css/skel.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/assets/css/style-xlarge.css" type="text/css"/>
        <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/custom.css" rel="stylesheet"/>
        <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/futura.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
        <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/main.css" rel="stylesheet"/>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
        <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/footercss.css" rel="stylesheet" type="text/css">
    </head>

<body>
    
<?php wp_head(); ?>
<?php require('menu.php') ?>