<?php get_header(); ?>

     <?php 
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post(); ?> 
                    <div id="<?php echo $post->post_name; ?>">
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                    </div>
            <?php } 
        } 
    ?> 
    
<?php get_footer(); ?>