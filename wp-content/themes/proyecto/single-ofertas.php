<?php get_header(); ?>

<?php 
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post(); ?> 
            
                    <br />
                    <div id="contenido" class="row">

                        <br>

                         <?php 

                            $date_inicio = get_field('fecha_inicio', false, false); 
                            $date_inicio = new DateTime($date_inicio);

                        ?>

                        <div class="off_izq">

                            <div class="title"><h2><?php the_title(); ?></h2></div>
                            <div class="control-group">
                                Lugar de encuentro: <?php the_field('lugar_inicio'); ?>
                            </div>
                            <div class="control-group">
                                Lugar de la actividad: <?php the_field('lugar_final'); ?>
                            </div>
                            <div class="control-group">
                                Hora: <?php the_field('hora_inicio'); ?> - <?php the_field('hora_final'); ?>
                            </div>
                            <div class="control-group">
                                Día: <?php echo $date_inicio->format('j M Y'); ?>
                            </div>
                            <div class="control-group">
                                Descripción: <?php the_title(); ?>
                            </div>
                            
                        </div>
                    </div>

                <?php } 
            } 
    ?>  

<?php get_footer(); ?>