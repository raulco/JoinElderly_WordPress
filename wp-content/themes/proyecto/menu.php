 <!-- Header -->
    <div  ng-controller="menuCtrl">
        <header id="header">
            <h1>
                <img class="logo" src="<?php echo get_stylesheet_directory_uri();?>/assets/media/Logo.png" alt="70" width="70" />
                <a class="titulomain" href="#/">JoinElderly</a>
            </h1>
            
            <nav id="nav">
                <ul>
                    <?php wp_nav_menu (array(

                    'theme_location' => 'menu_ppal',
                    'container' => false
                    )); ?>

                    <?php 
                    if ( is_user_logged_in() ) { 

                        global $current_user;
                        get_currentuserinfo();

                    ?>

                    <li><a href="<?php bloginfo('home'); ?>/acceder">Registrado como <?php echo $current_user->user_login; ?></a></li>

                    <?php } else { ?>

                        <li><a href="<?php bloginfo('home'); ?>/acceder">Acceder</a></li>
                        
                    <?php } ?>
                </ul>
            </nav>
        </header>
    </div>