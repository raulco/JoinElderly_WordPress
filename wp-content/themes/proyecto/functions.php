<?php

register_nav_menus(array(
    'menu_ppal' => 'Menu Ppal',
    'footer_menu' => 'Footer Menu',
    'conocenos_menu' => 'Conocenos Menu',
    'Necesitas_menu' => 'Necesitas Menu',
    'Condiciones_menu' => 'Condiciones'
));

add_theme_support('post-thumbnails');

// Register Custom Post Type Slider
function custom_slides() {

    $labels = array(
        'name' => _x('slides', 'Post Type General Name', 'joinelderly'),
        'singular_name' => _x('slide', 'Post Type Singular Name', 'joinelderly'),
        'menu_name' => __('Slides', 'joinelderly'),
        'name_admin_bar' => __('Slides', 'joinelderly'),
        'archives' => __('Item Archives', 'joinelderly'),
        'parent_item_colon' => __('Parent Item:', 'joinelderly'),
        'all_items' => __('All Items', 'joinelderly'),
        'add_new_item' => __('Add New Item', 'joinelderly'),
        'add_new' => __('Add New', 'joinelderly'),
        'new_item' => __('New Item', 'joinelderly'),
        'edit_item' => __('Edit Item', 'joinelderly'),
        'update_item' => __('Update Item', 'joinelderly'),
        'view_item' => __('View Item', 'joinelderly'),
        'search_items' => __('Search Item', 'joinelderly'),
        'not_found' => __('Not found', 'joinelderly'),
        'not_found_in_trash' => __('Not found in Trash', 'joinelderly'),
        'featured_image' => __('Featured Image', 'joinelderly'),
        'set_featured_image' => __('Set featured image', 'joinelderly'),
        'remove_featured_image' => __('Remove featured image', 'joinelderly'),
        'use_featured_image' => __('Use as featured image', 'joinelderly'),
        'insert_into_item' => __('Insert into item', 'joinelderly'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'joinelderly'),
        'items_list' => __('Items list', 'joinelderly'),
        'items_list_navigation' => __('Items list navigation', 'joinelderly'),
        'filter_items_list' => __('Filter items list', 'joinelderly'),
    );
    $args = array(
        'label' => __('slide', 'joinelderly'),
        'description' => __('Post Type Description', 'joinelderly'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('slides', $args);
}

add_action('init', 'custom_slides', 0);

// Register Custom Post Type Ofertas
function custom_ofertas() {

    $labels = array(
        'name' => _x('ofertas', 'Post Type General Name', 'joinelderly'),
        'singular_name' => _x('oferta', 'Post Type Singular Name', 'joinelderly'),
        'menu_name' => __('Ofertas', 'joinelderly'),
        'name_admin_bar' => __('Ofertas', 'joinelderly'),
        'archives' => __('Item Archives', 'joinelderly'),
        'parent_item_colon' => __('Parent Item:', 'joinelderly'),
        'all_items' => __('All Items', 'joinelderly'),
        'add_new_item' => __('Add New Item', 'joinelderly'),
        'add_new' => __('Add New', 'joinelderly'),
        'new_item' => __('New Item', 'joinelderly'),
        'edit_item' => __('Edit Item', 'joinelderly'),
        'update_item' => __('Update Item', 'joinelderly'),
        'view_item' => __('View Item', 'joinelderly'),
        'search_items' => __('Search Item', 'joinelderly'),
        'not_found' => __('Not found', 'joinelderly'),
        'not_found_in_trash' => __('Not found in Trash', 'joinelderly'),
        'featured_image' => __('Featured Image', 'joinelderly'),
        'set_featured_image' => __('Set featured image', 'joinelderly'),
        'remove_featured_image' => __('Remove featured image', 'joinelderly'),
        'use_featured_image' => __('Use as featured image', 'joinelderly'),
        'insert_into_item' => __('Insert into item', 'joinelderly'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'joinelderly'),
        'items_list' => __('Items list', 'joinelderly'),
        'items_list_navigation' => __('Items list navigation', 'joinelderly'),
        'filter_items_list' => __('Filter items list', 'joinelderly'),
    );
    $args = array(
        'label' => __('oferta', 'joinelderly'),
        'description' => __('Post Type Description', 'joinelderly'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('ofertas', $args);
}

add_action('init', 'custom_ofertas', 0);


/* * **** Registro ***** */

function register_user() {

    /* Registramos el usuario con los datos obtenidos en el formulario */

    $user_id = wp_insert_user(
            array(
                'user_login' => $_POST['email'],
                'user_pass' => $_POST['password'],
                'first_name' => $_POST['nombre'],
                'last_name' => $_POST['apellidos'],
                'user_email' => $_POST['email'],
                'nickname' => $_POST['nombre'] . ' ' . $_POST['apellidos'],
                'role' => 'editor'
            )
    );

    redirect(get_bloginfo('url'));
}

function redirect($url) {
    echo '<script type="text/javascript">window.top.location="' . $url . '";</script>';
    exit;
}

function remove_ul($menu) {
    return preg_replace(array('#^<ul[^>]*>#', '#</ul>$#'), '', $menu);
}

add_filter('wp_nav_menu', 'remove_ul');
?>