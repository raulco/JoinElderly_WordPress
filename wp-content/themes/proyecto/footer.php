        <!-- Footer -->
        <footer id="footer">
            <div class="container">
                <section class="links">
                    <div class="row">
                        <section class="3u 6u(medium) 12u$(small)">
                            <h3>Conócenos</h3>
                            <ul class="unstyled">
                                <li><a href="<?php bloginfo('url'); ?>/#">¿Quiénes somos?</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/#">¿Como funciona?</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/trabaja">Trabaja con nosotros</a></li>
                            </ul>
                        </section>
                        <section class="3u 6u(medium) 12u$(small)">
                            <h3>¿Necesitas ayuda?</h3>
                            <ul class="unstyled">
                                <li><a href="<?php bloginfo('url'); ?>/contacto">Contacto</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/#">Preguntas Frecuentes</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/#">Seguros JoinElderly</a></li>
                            </ul>
                        </section>
                        <section class="3u 6u(medium) 12u$(small)">
                            <h3>Condiciones</h3>
                            <ul class="unstyled">
                                <li><a href="<?php bloginfo('url'); ?>/terminos">Términos y Condiciones</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/politica">Política de Privacidad</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/aviso">Aviso Legal</a></li>
                            </ul>
                        </section>
                    </div>
                </section>
                <div class="row">
                    <div class="8u 12u$(medium)">
                        <ul class="copyright">
                            <li>&copy; 2016 JoinElderly. All rights reserved.</li>
                        </ul>
                    </div>
                    <div class="4u$ 12u$(medium)">
                        <ul class="icons">
                            <li>
                                <a class="icon rounded fa-facebook" href="https://www.facebook.com/profile.php?id=100010986072513"><span class="label">Facebook</span></a>
                            </li>
                            <li>
                                <a class="icon rounded fa-twitter" href="https://twitter.com/JoinElderly"><span class="label">Twitter</span></a>
                            </li>
                            <li>
                                <a class="icon rounded fa-google-plus" href="https://plus.google.com/u/1/115341429311812683133"><span class="label">Google+</span></a>
                            </li>
                        </ul>
                        <a href="#" class="scrollup">Scroll</a>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
