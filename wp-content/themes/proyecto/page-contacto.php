<?php

/*
Template Name: Contacto
*/

get_header();?>

<script src="<?php echo get_stylesheet_directory_uri();?>/assets/js/bootstrap-button.js"></script>


<div class="container">
	<br/>
	<br/>
	<div class="form-contact">
		<h2><?php the_title(); ?></h2>
   	 	<?php echo do_shortcode('[contact-form-7 id="38" title="Contact form 1"]');?>
	</div>

</div> <!-- /container -->
<div class="container">
    <section class="contacto">
        <div class="6u 12u(3)">
            <br>
            <h4>O bien puedes ponerte en contacto con nosotros mediante: </h4>
            <br>
            <ul class="icons">
                <li><a href="https://mail.google.com/"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/media/gmail.png" class="gmail"></a><span class="correogmail">joinelderly@gmail.com</span></li>
                <li><a href="https://twitter.com/JoinElderly" class="icon rounded fa-twitter"><span class="label">Twitter</span></a><span class="redsocial">@JoinElderly</span></li>
                <li><a href="https://www.facebook.com/profile.php?id=100010986072513" class="icon rounded fa-facebook"><span class="label">Facebook</span></a><span class="redsocial">facebook.com/JoinElderly</span></li>
            </ul>
        </div>

    </section>
</div>

<?php get_footer(); ?>