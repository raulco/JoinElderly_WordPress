<?php get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style_slider.css" />

<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <?php
            // WP_Query arguments
            $args = array(
                'post_type' => array('slides'),
            );

            // The Query
            $slides = new WP_Query($args);

            // The Loop
            if ($slides->have_posts()) {
                while ($slides->have_posts()) {
                    $slides->the_post();

                    $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    ?>
                    <li><img src="<?php echo $url; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" id="wows<?php the_ID(); ?>"/></li>

                <?php
                }
            }

            // Restore original Post Data
            wp_reset_postdata();
            ?>   
        </ul>
    </div>
    <!--<div class="ws_bullets">
            <div>
    <?php
    $i = 1;
    while ($slides->have_posts()) {
        $slides->the_post();

        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
        ?>
                        <a href="#" title="<?php the_title(); ?>"><span><img src="<?php echo $url[0]; ?>" alt="<?php the_title(); ?>"/><?php echo $i ?></span></a>
        <?php
        $i++;
    }
    wp_reset_postdata();
    ?>
            </div>
        </div>-->
    <div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/wowslider.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/script.js"></script>

<!-- One -->
<section id="one" class="wrapper style1 special">
    <div class="container">
        <header class="major">
            <h2>JoinElderly.com</h2>
            <p>Tu página para buscar, conocer y encontrar una nueva forma de atención.</p>
        </header>
        <div class="row 150%">
            <div class="4u 12u$(medium)">
                <section class="box">
                    <a href="<?php bloginfo('url'); ?>/ofertas"> <img id="img_contact" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/media/buscar.jpg" alt="120" width="120"></a>
                    <h3>Busca acompañante</h3>
                    <p>Encuentra las mejores personas para que te acompañen o unete a un grupo con más gente de tu edad.</p>
                </section>
            </div>
            <div class="4u 12u$(medium)">
                <section class="box">
                    <a href="<?php bloginfo('url'); ?>/trabaja"> <img id="img_contact" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/media/trabaja.png" alt="120" width="120"></a>
                    <h3>Únete a nosotros</h3>
                    <p>No te quedes encerrado en casa, únete a nosotros, sal a la calle y empieza a relacionarte con gente, nunca ha sido tan fácil como ahora.</p>
                </section>
            </div>
            <div class="4u$ 12u$(medium)">
                <section class="box">
                    <a href="<?php bloginfo('url'); ?>/contacto"><img id="img_contact" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/media/logo-contact.png" alt="120" width="120"></a>
                    <h3>Contacta con nosotros</h3>
                    <p>Para cualquier duda, sugerencia o problema solo necesitas rellenar estos campos y ya estará todo hecho, cuanto más simple, mejor.</p>
                </section>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>